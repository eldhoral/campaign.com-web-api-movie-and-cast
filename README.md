# Install this library #


* "body-parser": "^1.19.0"
* "express": "^4.17.1"
* "mysql": "^2.18.1"

### Link ###

* localhost:3000/api/movies
* localhost:3000/api/movies:id
* localhost:3000/api/casts
* localhost:3000/api/casts:id
* localhost:3000/api/detailmovie
* localhost:3000/api/detailmovie:id